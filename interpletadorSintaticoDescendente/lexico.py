def analiseLexica(fonte):
    palavras_reservadas = ['if', 'then', 'else', 'end', 'repeat', 'until', 'read', 'write']
    operadores_logicos = ['<', '=']
    operadores_soma = ['+', "-"]
    operadores_fator = ['*', "/"]
    atribuicao = ':='
    fim_de_comando = ';'
    abre_parenteses = '('
    fecha_parenteses = ')'
    tokens = []

    arquivo = open(fonte, 'r')

    linhas = arquivo.readlines()

    linha_atual = 0
    linhas_totais = len(linhas)

    msg_erro = True
    msg = ""

    while linha_atual < linhas_totais:
        for l in linhas[linha_atual].split():
            if l.isalnum():
                if l in palavras_reservadas:
                    tokens.append((l, "palavra reservada", linha_atual + 1))
                elif l.isdigit():
                    tokens.append((l, "numero", linha_atual + 1))
                else:
                    tokens.append((l, "identificador", linha_atual + 1))
            elif l in operadores_logicos:
                tokens.append((l, "operador logico", linha_atual + 1))
            elif l in operadores_soma:
                tokens.append((l, "operado soma", linha_atual + 1))
            elif l in operadores_fator:
                tokens.append((l, "operador fator", linha_atual + 1))
            elif l == atribuicao:
                tokens.append((l, "atribuicao", linha_atual + 1))
            elif l == abre_parenteses:
                tokens.append((l, "abre parenteses", linha_atual + 1))
            elif l == fecha_parenteses:
                tokens.append((l, "fecha parenteses", linha_atual + 1))
            elif l == fim_de_comando:
                tokens.append((l, "fim do comando", linha_atual + 1))
            else:
                msg = 'token desconhecido na linha ', linha_atual, '. Simbolo:', l
                msg_erro = False
                break

        linha_atual += 1

    if msg_erro:
        return tokens
    else:
        return msg
