import lexico as lex

tokens = lex.analiseLexica("principal.txt")
tokens.insert(len(tokens) - 1, ('$', 'simbolo final', 'ultima linha'))

pilha = []
pilha.insert(0, "$")
pilha.insert(0, "program")

terminais = ['identifier', 'number', 'if', 'then', 'else', 'end', 'repeat', 'until', 'read', 'write', '<', '=', '+',
             "-", '*', "/", ':=', '(', ')', ';']

tabela = {
    "program": {
        "identifier": "stmt-sequence", "number": "erro", "if": "stmt-sequence", "then": "erro", "end": "erro",
        "else": "erro", "repeat": "stmt-sequence", "until": "erro", ":=": "erro", "read": "stmt-sequence",
        "write": "stmt-sequence", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": "erro", ")": "erro", ";": "erro", "$": "erro"
    },
    "stmt-sequence": {
        "identifier": "stmt-sequence", "number": "erro", "if": "stmt-sequence", "then": "erro", "end": "erro",
        "else": "erro", "repeat": "stmt-sequence", "until": "erro", ":=": "erro", "read": "stmt-sequence",
        "write": "stmt-sequence", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": "erro", ")": "erro", ";": "erro", "$": "erro"
    },
    "stmt-sequence'": {
        "identifier": "erro", "number": "erro", "if": "erro", "then": "erro", "end": '&',
        "else": '&', "repeat": "erro", "until": '&', ":=": "erro", "read": "erro",
        "write": "erro", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": "erro", ")": "erro", ";": [";", "statement", "stmt-sequence'"], "$": '&'
    },
    "statement": {
        "identifier": "assing-stmt", "number": "erro", "if": "if-stmt", "then": "erro", "end": "erro",
        "else": "erro", "repeat": "repeat-stmt", "until": "erro", ":=": "erro", "read": "read-stmt",
        "write": "write-stmt", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": "erro", ")": "erro", ";": "erro", "$": "erro"
    },
    "if-stmt": {
        "identifier": "erro", "number": "erro", "if": ["if", "exp", "then", "stmt-sequence", "end"], "then": "erro",
        "end": "erro",
        "else": "erro", "repeat": "erro", "until": "erro", ":=": "erro", "read": "erro",
        "write": "erro", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": "erro", ")": "erro", ";": "erro", "$": "erro"
    },
    "assing-stmt": {
        "identifier": ["identifier", ":=", "exp"], "number": "erro", "if": "erro", "then": "erro", "end": "erro",
        "else": "erro", "repeat": "erro", "until": "erro", ":=": "erro", "read": "erro",
        "write": "erro", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": "erro", ")": "erro", ";": "erro", "$": "erro"
    },
    "repeat-stmt": {
        "identifier": "erro", "number": "erro", "if": "erro", "then": "erro", "end": "erro",
        "else": "erro", "repeat": ["repeat", "stmt-sequence", ""], "until": "erro", ":=": "erro", "read": "erro",
        "write": "erro", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": "erro", ")": "erro", ";": "erro", "$": "erro"
    },
    "read-stmt": {
        "identifier": "erro", "number": "erro", "if": "erro", "then": "erro", "end": "erro",
        "else": "erro", "repeat": "erro", "until": "erro", ":=": "erro", "read": ["read", "identifier"],
        "write": "erro", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": "erro", ")": "erro", ";": "erro", "$": "erro"
    },
    "write-stmt": {
        "identifier": "erro", "number": "erro", "if": "erro", "then": "erro", "end": "erro",
        "else": "erro", "repeat": "erro", "until": "erro", ":=": "erro", "read": "erro",
        "write": ["write", "exp"], "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": "erro", ")": "erro", ";": "erro", "$": "erro"
    },
    "exp": {
        "identifier": ["simple-exp", "comparation-op", "simple-exp"],
        "number": ["simple-exp", "comparation-op", "simple-exp"], "if": "erro", "then": "erro", "end": "erro",
        "else": "erro", "repeat": "erro", "until": "erro", ":=": "erro", "read": "erro",
        "write": "erro", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": ["simple-exp", "comparation-op", "simple-exp"], ")": "erro", ";": "erro", "$": "erro"
    },
    "comparation-op": {
        "identifier": "erro", "number": "erro", "if": "erro", "then": "erro", "end": "erro",
        "else": "erro", "repeat": "erro", "until": "erro", ":=": "erro", "read": "erro",
        "write": "erro", "<": "erro", "=": "erro", "+": "+", "-": "-", "*": "erro", "/": "erro",
        "(": "erro", ")": "erro", ";": "erro", "$": "erro"
    },
    "simple-exp": {
        "identifier": ["term", "simple-exp'"], "number": ["term", "simple-exp"], "if": "erro", "then": "erro",
        "end": "erro",
        "else": "erro", "repeat": "erro", "until": "erro", ":=": "erro", "read": "erro",
        "write": "erro", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": ["term", "simple-exp'"], ")": "erro", ";": "erro", "$": "erro"
    },
    "simple-exp'": {
        "identifier": "erro", "number": "erro", "if": "erro", "then": "&", "end": "&",
        "else": "&", "repeat": "erro", "until": "&", ":=": "erro", "read": "erro",
        "write": "erro", "<": "&", "=": "&", "+": ["addop", "term", "simple-exp'"],
        "-": ["addop", "term", "simple-exp'"], "*": "erro", "/": "erro",
        "(": "erro", ")": "&", ";": "&", "$": "erro"
    },
    "addop": {
        "identifier": "erro", "number": "erro", "if": "erro", "then": "erro", "end": "erro",
        "else": "erro", "repeat": "erro", "until": "erro", ":=": "erro", "read": "erro",
        "write": "erro", "<": "erro", "=": "erro", "+": "+", "-": "-", "*": "erro", "/": "erro",
        "(": "erro", ")": "erro", ";": "erro", "$": "erro"
    },
    "term": {
        "identifier": ["factor", "term"], "number": ["factor", "term"], "if": "erro", "then": "erro", "end": "erro",
        "else": "erro", "repeat": "erro", "until": "erro", ":=": "erro", "read": "erro",
        "write": "erro", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": ["factor", "term"], ")": "erro", ";": "erro", "$": "erro"
    },
    "term'": {
        "identifier": "erro", "number": "erro", "if": "erro", "then": "&", "end": "&",
        "else": "&", "repeat": "erro", "until": "&", ":=": "erro", "read": "erro",
        "write": "erro", "<": "&", "=": "&", "+": "&", "-": "&", "*": ["mulop", "factor", "term'"],
        "/": ["mulop", "factor", "term'"],
        "(": "erro", ")": "&", ";": "erro", "$": "erro"
    },
    "mulop": {
        "identifier": "erro", "number": "erro", "if": "erro", "then": "erro", "end": "erro",
        "else": "erro", "repeat": "erro", "until": "erro", ":=": "erro", "read": "erro",
        "write": "erro", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "*", "/": "/",
        "(": "erro", ")": "erro", ";": "erro", "$": "erro"
    },
    "factor": {
        "identifier": "identifier", "number": "number", "if": "erro", "then": "erro", "end": "erro",
        "else": "erro", "repeat": "erro", "until": "erro", ":=": "erro", "read": "erro",
        "write": "erro", "<": "erro", "=": "erro", "+": "erro", "-": "erro", "*": "erro", "/": "erro",
        "(": ["(", "exp", ")"], ")": "erro", ";": "erro", "$": "erro"
    }
}


def ident(sequencia):
    if str(sequencia).isalpha():
        return "identifier"
    elif str(sequencia).isnumeric():
        return "number"
    else:
        return sequencia


while True:
    entrada = 0
    topo_pilha = pilha[0]
    curso = tokens[entrada]
    if topo_pilha in terminais or topo_pilha == '$':
        print("entro aqui")
        print(pilha)
        if topo_pilha == ident(curso[0]):
            pilha.pop(0)
            topo_pilha = pilha[0]
            entrada += 1
            curso = tokens[entrada]
        else:
            print("Erro, fase 1 na linha " + str(curso[2]) + ". Esperado era " + topo_pilha)
    elif tabela[topo_pilha][ident(curso[0])] != '&':
        print(pilha)
        pilha.pop(0)
        elemento = tabela[topo_pilha][ident(curso[0])]
        if elemento is list:
            for e in elemento.reverse():
                pilha.insert(0, e)
        else:
            pilha.insert(0, elemento)
    else:
        print("Erro,fase 2 na linha " + str(curso[2]) +
              ". Esperado era " + topo_pilha)
    if topo_pilha == '$':
        print("entrada validada")
        break
